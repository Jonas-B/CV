window.tabs = {
	current: undefined,
	init: function(){
		this.tabsWrapper = document.querySelector('.tabs');
		this.tabs = this.tabsWrapper.querySelectorAll('.tab');

		/*this.tabs.forEach(function(tab){
			tab.addEventListener('click', function(){
				this.change(tab.getAttribute('target'));
			}.bind(this))
		}.bind(this))*/

		console.log(window.kernel);

		this.change(window.kernel.route, true);
	},
	change: function(target, force){
		if(this.current) this.current.className = this.current.className.split('current')[0].trim();
		this.current = this.tabsWrapper.querySelector('.tab[target='+target+']');
		if(force) this.current.className += ' current';
	},
	active: function(target, force){
		this.current = this.tabsWrapper.querySelector('.tab[target='+target+']');
		if(force) this.current.className += ' current';
	}
}

tabs.init()