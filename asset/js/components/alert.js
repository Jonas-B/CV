class Alert{
	constructor(state, message){
		this.wrap = document.querySelector('.alert-wrap');

		this.state = state;
		this.message = message;

		this.create();
	}

	pullDefaultMessage(){
		switch(this.state){
			case 'check': return 'Yay ! Success !';
			case 'warning': return 'Hmm, something doesn\'t work as expected.';
			case 'error': return 'An error occured. Sorry for that.'
		}
	}

	buildHTML(){
		this.node = document.createElement('div');
		var nodeIcon = document.createElement('div');
		var nodeMessage = document.createElement('div');

		this.node.className = 'alert ' + this.state;
		nodeIcon.className = 'icon';
		nodeIcon.textContent = this.state;
		nodeMessage.className = 'message';
		nodeMessage.innerHTML = this.message || this.pullDefaultMessage();

		this.node.title = nodeMessage.textContent;
		this.node.append(nodeIcon);
		this.node.append(nodeMessage);
	}

	create(){
		this.buildHTML();

		this.node.addEventListener('click', this.destroy.bind(this));

		if(this.wrap){
			this.wrap.append(this.node);
			requestAnimationFrame(function(){
				this.node.className += ' active';
				setTimeout(this.destroy.bind(this), 3000);
			}.bind(this));
		}
		else{
			console.warn('Cannot create an alert, wrapper not found')
		}
	}

	destroy(){
		this.node.className += ' removed';
		setTimeout(function(){ this.node.remove() }.bind(this), 3000);
	}
}

module.exports = Alert;