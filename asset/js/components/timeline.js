import moment from 'moment';

class Timeline{
	constructor(opt){
		this.opt = opt;

		this.line = {
			size: 100
		}

		this.date = {
			start: undefined,
			end: moment(),
			labels: 0
		}


		for(let key in this.opt.data){
			this.opt.data[key].forEach(function(e){
				if(moment(e.date.start).isBefore(this.date.start)){
					this.date.start = moment(e.date.start)
				}
			}.bind(this))
		}

		this.wrap = document.querySelector('.timeline');

		this.wrap.append(this.buildLeft());
		this.wrap.append(this.buildLine());
		this.wrap.append(this.buildRight());
	}

	topGutter(e, adjust = 0){
		let end = moment(this.date.end).clone().startOf('month');
		let start = moment(e.date.end).clone().startOf('month');

		return end.diff(start, 'months') < 0 ? 11 : (end.diff(start, 'months') + 1) * this.line.size + (end.diff(start, 'years') * 22) + 33 + adjust;
	}

	heightGutter(e){
		let end = moment(e.date.end).clone().startOf('month');
		let start = moment(e.date.start).clone().startOf('month');

		end = this.date.end.diff(end, 'months') < 0 ? this.date.end.clone().add(1, 'months').startOf('month') : end;

		return (end.diff(start, 'months')) * this.line.size  + (end.diff(start, 'years')* 22); 
	}

	buildLeft(){
		let node = document.createElement('div');
		node.className = 'left';

		this.opt.data.left.forEach(function(e){
			node.append(this.buildSet(e, 'left'));
		}.bind(this));

		return node;
	}

	buildRight(){
		let node = document.createElement('div');
		node.className = 'right';

		this.opt.data.right.forEach(function(e){
			node.append(this.buildSet(e, 'right'));
		}.bind(this));

		return node;
	}

	buildSet(set, side){
		let future = this.date.end.clone().startOf('month').diff(moment(set.date.end).clone().startOf('month'), 'months') < 0;

		let wrap = document.createElement('div');
		wrap.className = 'set'; 
		wrap.style.top = this.topGutter(set) + 'px'; 
		wrap.style.height = this.heightGutter(set) + 'px'; 

		let content = document.createElement('div');
		content.className = 'content';

		let title = document.createElement('div');
		title.className = 'title';
		title.innerHTML = set.title;

		let body = document.createElement('div');
		body.className = 'body';

		let infos = document.createElement('div');
		infos.className = 'tags';
		infos.innerHTML = '<div class="tag"><i class="icon">home</i> ' + set.subtitle.company + '</div><div class="tag"><i class="icon">location_on</i> ' + set.subtitle.location + '</div><div class="tag"><i class="icon">access_time</i>' + moment(set.date.start).format("L") + ' - ' + moment(set.date.end).format("L") + '</div>';

		let text = document.createElement('div');
		text.className = 'text';
		text.innerHTML = set.body.substr(0, 150) + (set.body.length > 150 ? ' ...' : '');

		let more = document.createElement('a');
		more.className = 'link spf-link';
		more.innerHTML = 'See more';
		more.href = set.more;

		body.append(infos);
		body.append(text);
		body.append(more);

		let fill = document.createElement('div');
		fill.className = 'fill line';

		let select = document.createElement('div');
		select.className = 'selector' + (future ? ' future' : '');

		let dates = document.createElement('div');
		dates.className = 'dates';

		let end = document.createElement('div');
		end.className = 'end' + (future ? ' future' : '');
		end.textContent = moment(set.date.end).format('MMMM Y');

		let start = document.createElement('div');
		start.className = 'start';
		start.textContent = moment(set.date.start).format('MMMM Y');

		content.append(title);
		content.append(body);

		select.append(dates);
		dates.append(end);
		dates.append(start);

		if(side == 'left'){
			wrap.append(content);
			wrap.append(fill);
			wrap.append(select);
		}
		else{
			wrap.append(select);
			wrap.append(fill);
			wrap.append(content);
		}
		return wrap;
	}

	buildLine(){

		let line = document.createElement('div');
		line.className = 'line';

		line.append(this.buildYear(this.date.end.format("dddd, MMMM Do")));
		line.append(this.buildLinePart(this.date.end.month()+1));

		let date = moment(this.date.end);
		while(date.isAfter(this.date.start)){
			line.append(this.buildYear(date.year()));
			line.append(this.buildLinePart(12));

			date = date.subtract(1, 'years')
		}
		line.append(this.buildYear(date.year()));

		this.opt.data.event.forEach(function(e){
			line.append(this.buildEvent(e));
		}.bind(this))

		return line;
	}

	buildEvent(event){
		let wrap = document.createElement('div');
		wrap.className = 'event content'; 
		wrap.style.top = this.topGutter(event, -25) + 'px'; 

		let date = document.createElement('div');
		date.className = 'date';
		date.textContent = moment(event.date.end).format('MMMM Y');

		let label = document.createElement('div');
		label.className = 'label';
		label.textContent = event.label;

		let icon = document.createElement('i');
		icon.className = 'icon';
		icon.textContent = event.icon;

		let body = document.createElement('div');
		body.className = 'body';

		let infos = document.createElement('div');
		infos.className = 'tags';
		infos.innerHTML = '<div class="tag"><i class="icon">home</i> ' + event.subtitle.company + '</div><div class="tag"><i class="icon">location_on</i> ' + event.subtitle.location + '</div><div class="tag"><i class="icon">access_time</i>' + moment(event.date.end).format("L") + '</div>';

		let text = document.createElement('div');
		text.className = 'text';
		text.innerHTML = event.body.substr(0, 150) + (event.body.length > 150 ? ' ...' : '');

		let more = document.createElement('a');
		more.className = 'link spf-link';
		more.innerHTML = 'See more';
		more.href = event.more;

		body.append(infos);
		body.append(text);
		body.append(more);

		wrap.append(icon);
		wrap.append(body);
		wrap.append(date);
		wrap.append(label);

		return wrap;
	}

	buildYear(content){
		this.date.labels += 1;
		let node = document.createElement('div');
		node.className = 'year';
		node.textContent = content;
		return node;
	}

	buildLinePart(nb){
		let node = document.createElement('div');
		node.className = 'part';
		node.style.height = (nb*this.line.size) + 'px';
		return node;
	}
}

window.Timeline = Timeline;