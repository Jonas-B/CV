window.load = {
	content: document.querySelector('#content'),
	init: function(){
		this.content.firstChild.className += 'content-leave';
	},
	move: function(move){

	},
	end: function(){
		requestAnimationFrame(function(){
			setTimeout(function(){
				this.content.firstChild.style.opacity = 1;
				this.content.firstChild.style.transform = 'translateY(0px)';
			}.bind(this), 100)
			
		}.bind(this))
	}
}

window.addEventListener('spfclick', function (e) {
    load.init();
});


window.addEventListener('spfrequest', function (e) {
    load.move(33);
});


window.addEventListener('spfprocess', function (e) {
    load.move(66);
});


window.addEventListener('spfdone', function (e) {
    load.end();
});