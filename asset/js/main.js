spf.init();
spf.load(window.kernel.load, { onProcess: function(){
	window.load.end();
}});

import './util/polyfill'
import './components/tabs'
import './util/clip';
import './components/spf-load';
import './components/timeline';