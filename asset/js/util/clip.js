import Alert from '../components/alert';

var clippers = document.querySelectorAll('.clipboard');
clippers.forEach(function(clipper){
	clipper.addEventListener(clipper.getAttribute('clip-type'), function(){ 
		clip(clipper.querySelector('.clip-target').textContent) 
	});
})

var clip = function(text){
	var tmp = document.createElement('textarea');
	tmp.textContent = text;
	document.body.append(tmp);
	tmp.select();

	if(document.execCommand('copy')) new Alert('check', 'Successfuly sended to your clipboard !')
	else new Alert('warning')

	tmp.remove();
}

module.exports = clip;