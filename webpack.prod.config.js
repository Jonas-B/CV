var webpack = require('webpack');
var config = require('./webpack.config');

module.exports = {
    entry: config.entry,
    output: {
        path: config.output.path,
        filename: config.output.filename
    },
    plugins: [
		new webpack.optimize.DedupePlugin(),
		new webpack.optimize.UglifyJsPlugin(),
		new webpack.optimize.AggressiveMergingPlugin()
    ]
};