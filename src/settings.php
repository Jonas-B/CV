<?php 
	return [
		"settings" => [
			'displayErrorDetails' 		=> true,
			'addContentLengthHeader' 	=> false,

			'db' => [
				'host'   => "localhost",
				'user'   => "user",
				'pass'   => "password",
				'dbname' => "exampleapp",
			]
		]
	];
?>