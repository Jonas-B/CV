<?php 
	namespace App\Controllers;

	class PublicController extends KernelController{
		public function home($req, $res) {
			return $this->view->render($res, 'pages/home.twig');
		}

		public function timeline($req, $res) {
			$this->initTwig();
			$this->twig['route'] = 'timeline';

			return $this->handleDefault($req, $res);
		}

		public function skills($req, $res) {
			$this->initTwig();
			$this->twig['route'] = 'skills';

			return $this->handleDefault($req, $res);
		}

		public function projects($req, $res) {
			$this->initTwig();
			$this->twig['route'] = 'projects';

			return $this->handleDefault($req, $res);
		}

		public function about($req, $res) {
			$this->initTwig();
			$this->twig['route'] = 'about';

			return $this->handleDefault($req, $res);
		}
	}

?>