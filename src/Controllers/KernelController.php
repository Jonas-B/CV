<?php 
	namespace App\Controllers;

	class KernelController extends SPFController{
		protected $twig = [];

		public function initTwig(){
			$this->twig['tabs'] = [
				[
					'route' => 'timeline',
					'icon' => 'timeline'
				],
				[
					'route' => 'skills',
					'icon' => 'code'
				],
				[
					'route' => 'projects',
					'icon' => 'work'
				],
				[
					'route' => 'about',
					'icon' => 'face'
				]
			];
		}
	}

?>