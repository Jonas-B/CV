<?php 
	namespace App\Controllers;

	class SPFController extends Controller{
		protected $root_template = 'layout/page.twig';

		protected function handleRequest($req, $res, $data){
			$spf = $req->getParams()['spf'] ?? false;
			if($spf){ return $this->JSONResponse($res, $data); }
			else{ return $this->HTMLResponse($res, $data); }
		}

		protected function handleDefault($req, $res, $data = []){
			$data['title'] = ucfirst($this->twig['route']);
			$data['head'] = $this->view->fetch('core/update.twig', $this->twig);
			$data['body'] = [ 'content' => $this->view->fetch('core/wrap.twig', array_merge($this->twig, [ 'template' => 'pages/' . $this->twig['route'] .'.twig' ])) ];
			return $this->handleRequest($req, $res, $data);
		}

		protected function JSONResponse($res, $data){
			$res = $res->withJson($data);
			return $res;
		}

		protected function HTMLResponse($res, $data){
			$this->twig['load'] = $this->router->pathFor($this->twig['route']);
			return $this->view->render($res, $this->root_template, $this->twig);
		}
	}
?>