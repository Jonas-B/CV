<?php 
	namespace App\Controllers;

	class Controller{
		private $container;

		public function __construct($container){
			$this->container = $container;
		}

		public function __get($var){
			return $this->container->$var;
		}
	}

?>