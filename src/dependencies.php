<?php 

	// Get container
	$container = $app->getContainer();

	// Register component on container
	$container['view'] = function ($container) {
		$view = new \Slim\Views\Twig('../src/Views', [
			'cache' => false//'path/to/cache'
		]);
		
		// Instantiate and add Slim specific extension
		$basePath = rtrim(str_ireplace('index.php', '', $container['request']->getUri()->getBasePath()), '/');

		class Extension extends Slim\Views\TwigExtension{
			public function getFunctions(){
				return array_merge(
					parent::getFunctions(),
					[
						new \Twig_SimpleFunction('asset', function ($asset) {
							if(substr($asset, 0, 1) !== '/' ){ $asset = '/' . $asset; }
						    return $this->baseUrl() . $asset;
						})
					]
				);
		    }
		}


		$view->addExtension(new Extension($container['router'], $basePath));

		/*$view->addFunction(new Slim\Views\TwigFunction('asset', function ($asset) {
		    var_dump($container['request']->getUri()->getBasePath());
		}));*/

		return $view;
	};

?>