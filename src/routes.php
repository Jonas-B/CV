<?php 

	$app->get('/', 			'\App\Controllers\PublicController:home')		->setName('home');
	$app->get('/timeline', 	'\App\Controllers\PublicController:timeline')	->setName('timeline');
	$app->get('/skills', 	'\App\Controllers\PublicController:skills')		->setName('skills');
	$app->get('/projects', 	'\App\Controllers\PublicController:projects')	->setName('projects');
	$app->get('/about', 	'\App\Controllers\PublicController:about')		->setName('about');

?>